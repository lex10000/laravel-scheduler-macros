# Макросы для Laravel Scheduler

### Пакет включает следующие макросы:
1) *everyXminutes(int: {variable})*, где {variable} - количество минут, через которое должен запускаться scheduler
1) *everyXhours(int: {variable})*, где {variable} - количество часов, через которое должен запускаться scheduler
1) *everyXdays(int: {variable})*, где {variable} - количество дней, через которое должен запускаться scheduler

### Пример использования (задача будет запускаться раз в 2 минуты):
 ```
   //App\Console\Kernel.php
   
   /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            Log::info('im starting at '.now() );
        })->everyXminutes(2);
    }
   ```

### Примечание:
Если передан параметр < 1, то будет автоматичекое приведение к 1 (минуте, часа и дня соответственно).


### Установка:
1) Добавить в composer.json проекта laravel следующие записи:
   ```
   "require": {
   ...
   "lex10000/laravel-scheduler-macros" : "^1.0.0"
   },
   ```
   ```
   "repositories": [
        ...
        {
            "type": "gitlab",
            "url": "https://gitlab.com/lex10000/laravel-scheduler-macros"
        }
    ]
2) composer update
