<?php

namespace Lex10000\LaravelSchedulerMacros;

class SchedulerMixin
{
    /**
     * Запускать Scheduler задачу каждые Х минут
     * @return callable
     */
    public function everyXminutes(): callable
    {
        return fn(int $minutes = 1) => $minutes < 1 ? $this->cron("*/1 * * * *") : $this->cron("*/$minutes * * * *");
    }

    /**
     * Запускать Scheduler задачу каждые Х часов
     * @return callable
     */
    public function everyXhours(): callable
    {
        return fn(int $hours = 1) => $hours < 1 ? $this->cron("0 */1 * * *") : $this->cron("0 */$hours * * *");
    }

    /**
     * Запускать Scheduler задачу каждые Х дней
     * @return callable
     */
    public function everyXdays(): callable
    {
        return fn(int $days = 1) => $days < 1 ? $this->cron("0 0 */1 * *"): $this->cron("0 0 */$days * *");
    }
}
