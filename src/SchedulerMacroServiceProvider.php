<?php
namespace Lex10000\LaravelSchedulerMacros;

use Illuminate\Console\Scheduling\CallbackEvent;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\ServiceProvider;
use Lex10000\LaravelSchedulerMacros\SchedulerMixin;

class SchedulerMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        CallbackEvent::mixin(new SchedulerMixin);
    }
}
